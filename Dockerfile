# build stage
FROM golang:1.14.3-alpine AS build-env
ADD . /src
RUN cd /src && go build -o umi-master

# final stage
FROM alpine
WORKDIR /app
COPY --from=build-env /src/umi-master /app/
COPY --from=build-env /src/wallet /app/wallet/
ENTRYPOINT ["./umi-master", "-bind=0.0.0.0:8080", "-testnet"]
