package jsonrpc

var methods = make(map[string]func(*Request, *Response))

func NewResponse() *Response {
	return &Response{
		Jsonrpc: "2.0",
	}
}

func (r *Response) SetId(id *string) {
	r.Id = id
}

func RegisterMethod(m string, f func(*Request, *Response)) {
	methods[m] = f
}

func CallMethod(req *Request, res *Response) {
	res.SetId(req.Id)

	method, ok := methods[req.Method]
	if !ok {
		res.Error = &ErrorObject{
			Code:    -32601,
			Message: "Method not found",
		}
		return
	}

	method(req, res)
}
