package jsonrpc

import "encoding/json"

type Request struct {
	Jsonrpc string          `json:"jsonrpc"`
	Method  string          `json:"method"`
	Params  json.RawMessage `json:"params"`
	Id      *string         `json:"id"`
}

type Response struct {
	Jsonrpc string       `json:"jsonrpc"`
	Result  interface{}  `json:"result,omitempty"`
	Error   *ErrorObject `json:"error,omitempty"`
	Id      *string      `json:"id"`
}

type ErrorObject struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
