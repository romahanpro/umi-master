package methods

import (
	"context"
	"encoding/json"
	"github.com/btcsuite/btcutil/bech32"
	"github.com/umi-top/umi-core/address"
	"umi-master/api/jsonrpc"
	"umi-master/storage/pg"
)

func init() {
	jsonrpc.RegisterMethod("getBalance", getBalance)
}

func getBalance(req *jsonrpc.Request, res *jsonrpc.Response) {
	params := &struct {
		Address  string `json:"address"`
		Detailed bool   `json:"detailed"`
	}{}

	if err := json.Unmarshal(req.Params, params); err != nil {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32602,
			Message: err.Error(),
		}
		return
	}

	if _, _, err := bech32.Decode(params.Address); err != nil {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32602,
			Message: err.Error(),
		}
		return
	}

	adr := address.FromBech32(params.Address).ToBytes()

	var confVal int64
	var conPerc int32
	var uncfVal int64
	var compVal *int64
	sql := "SELECT confirmed_value, confirmed_percent, unconfirmed_value, composite_value FROM get_address_balance($1)"
	if err := pg.Db.QueryRow(context.Background(), sql, adr).Scan(&confVal, &conPerc, &uncfVal, &compVal); err != nil {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32603,
			Message: err.Error(),
		}
		return
	}

	type response struct {
		Confirmed   int64  `json:"confirmed"`
		Unconfirmed int64  `json:"unconfirmed"`
		Percent     int32  `json:"interest"`
		Composite   *int64 `json:"composite,omitempty"`
	}

	rez := &response{
		Confirmed:   confVal,
		Unconfirmed: uncfVal,
		Percent:     conPerc,
		Composite:   compVal,
	}

	res.Result = rez
}
