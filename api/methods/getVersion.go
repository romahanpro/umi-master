package methods

import "umi-master/api/jsonrpc"

func init() {
	jsonrpc.RegisterMethod("getVersion", getVersion)
}

func getVersion(req *jsonrpc.Request, res *jsonrpc.Response) {
	res.Result = &struct {
		Version string `json:"version"`
	}{
		Version: "0.9.12",
	}
}
