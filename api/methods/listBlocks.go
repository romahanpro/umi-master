package methods

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"umi-master/api/jsonrpc"
	"umi-master/storage/pg"
)

func init() {
	jsonrpc.RegisterMethod("listBlocks", listBlocks)
}

func listBlocks(req *jsonrpc.Request, res *jsonrpc.Response) {
	params := &struct {
		Height uint64 `json:"height"`
	}{}

	if err := json.Unmarshal(req.Params, params); err != nil {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32602,
			Message: err.Error(),
		}
		return
	}

	sql := "select lo_get(height) from block where height >= $1 and confirmed is true order by height limit 100"
	rows, err := pg.Db.Query(context.Background(), sql, params.Height)

	if err != nil {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32603,
			Message: err.Error(),
		}
		return
	}

	defer rows.Close()

	blks := make([]string, 0, rows.CommandTag().RowsAffected())
	blk := make([]byte, 1000)

	for rows.Next() {
		if err = rows.Scan(&blk); err != nil {
			res.Error = &jsonrpc.ErrorObject{
				Code:    -32603,
				Message: err.Error(),
			}
			return
		}

		blks = append(blks, base64.StdEncoding.EncodeToString(blk))
	}

	res.Result = blks
}
