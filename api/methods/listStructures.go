package methods

import (
	"context"
	"github.com/umi-top/umi-core/address"
	"umi-master/api/jsonrpc"
	"umi-master/storage/pg"
)

func init() {
	jsonrpc.RegisterMethod("listStructures", listStructures)
}

func listStructures(req *jsonrpc.Request, res *jsonrpc.Response) {

	sql := `
select t.prefix,
       t.name,
       t.profit_percent,
       t.fee_percent,
       t.profit_address,
       t.fee_address,
       t.master_address,
       (t.z).value,
       (t.z).percent,
       t.h,
       t.ac
from (
         select s.prefix,
                s.name,
                s.profit_percent,
                s.fee_percent,
                s.profit_address,
                s.fee_address,
                s.master_address,
                get_structure_balance(s.version) as z,
                (select array_agg(address)
                 from structure_address
                 where version = s.version
                   and type = 'transit'
                   and deleted_at is null)       as h,
                ss.address_count                 as ac
         from structure_settings s
         left join structure_stats ss on s.prefix = ss.prefix
     ) as t;`
	rows, err := pg.Db.Query(context.Background(), sql)

	if err != nil {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32603,
			Message: err.Error(),
		}
		return
	}

	defer rows.Close()

	type Struct struct {
		Prefix           string    `json:"prefix"`
		Name             string    `json:"name"`
		DepositPercent   int32     `json:"deposit_percent"`
		ProfitPercent    int32     `json:"profit_percent"`
		FeePercent       int32     `json:"fee_percent"`
		FeeAddress       string    `json:"fee_address"`
		ProfitAddress    string    `json:"profit_address"`
		MasterAddress    string    `json:"master_address"`
		Balance          int64     `json:"balance"`
		TransitAddresses *[]string `json:"transit_addresses,omitempty"`
		AddressCount     int64     `json:"address_count"`
	}

	sts := make([]Struct, 0, rows.CommandTag().RowsAffected())

	var mstAdr []byte
	var prfAdr []byte
	var feeAdr []byte
	var tranzs [][]byte

	for rows.Next() {
		st := Struct{}

		if err = rows.Scan(&st.Prefix, &st.Name, &st.ProfitPercent, &st.FeePercent, &prfAdr, &feeAdr, &mstAdr, &st.Balance, &st.DepositPercent, &tranzs, &st.AddressCount); err != nil {
			res.Error = &jsonrpc.ErrorObject{
				Code:    -32603,
				Message: err.Error(),
			}
			return
		}

		st.MasterAddress = address.FromBytes(mstAdr).ToBech32()
		st.FeeAddress = address.FromBytes(feeAdr).ToBech32()
		st.ProfitAddress = address.FromBytes(prfAdr).ToBech32()

		if len(tranzs) > 0 {
			ss := make([]string, 0, len(tranzs))
			for _, tt := range tranzs {
				ss = append(ss, address.FromBytes(tt).ToBech32())
			}
			st.TransitAddresses = &ss
		}

		sts = append(sts, st)
	}

	res.Result = sts
}
