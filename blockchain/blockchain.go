package blockchain

import (
	"context"
	"encoding/base64"
	"github.com/umi-top/umi-core/block"
	"github.com/umi-top/umi-core/key"
	"github.com/umi-top/umi-core/transaction"
	"log"
	"os"
	"time"
	"umi-master/storage/pg"
)

func GenerateBlock(ctx context.Context) {

	mstKey := os.Getenv("KEY")
	if len(mstKey) == 0 {
		return
	}

	secKey, err := base64.StdEncoding.DecodeString(mstKey)
	if err != nil {
		log.Println("sec key: ", err)
		return
	}


	var cnt int64
	if err := pg.Db.QueryRow(ctx, "select count(*) from block where confirmed is false").Scan(&cnt); err != nil {
		return
	}
	if cnt > 0 {
		return
	}

	rows, err := pg.Db.Query(ctx, "select raw from mempool order by priority desc")
	if err != nil {
		return
	}

	defer rows.Close()

	blk := block.NewBlock()
	tx := transaction.NewTransaction()
	now := time.Now().Unix()
	balances := make(map[string]uint64)
	structures := make(map[uint16]bool)
	addresses := make(map[string]bool)

	var balance uint64
	var ok bool

	for rows.Next() {
		if blk.TxCount() >= 1000 {
			break
		}

		if err = rows.Scan(&tx.Bytes); err != nil {
			return
		}

		if err := pg.Db.QueryRow(ctx, "select exists (select 1 from transaction where hash = $1)", tx.Hash()).Scan(&ok); err != nil {
			return
		}

		if ok {
			continue
		}

		balance, ok = balances[string(tx.Sender().Bytes)]
		if !ok {
			if err := pg.Db.QueryRow(ctx, "select confirmed_value from get_address_balance($1::bytea, to_timestamp($2))", tx.Sender().Bytes, now).Scan(&balance); err != nil {
				return
			}
			balances[string(tx.Sender().Bytes)] = balance
		}

		switch tx.Version() {
		case transaction.Basic:
			if balance >= tx.Value() {
				if tx.Recipient().Version() == uint16(0x55A9) {
					balances[string(tx.Sender().Bytes)] = balance - tx.Value()
					blk.AppendTransaction(tx)

					continue
				} else {
					if err := pg.Db.QueryRow(ctx, "select exists (select 1 from structure_settings where version = $1)", tx.Recipient().Version()).Scan(&ok); err != nil {
						return
					}
					if ok {
						balances[string(tx.Sender().Bytes)] = balance - tx.Value()
						blk.AppendTransaction(tx)

						continue
					}
				}
			} else {
				continue
			}
		case transaction.CreateSmartContract:
			if tx.Recipient().Version() == uint16(0x55A9) {
				continue
			}

			if balance >= 50_000_00 {

				if err := pg.Db.QueryRow(ctx, "select exists (select 1 from structure_settings where version = $1)", tx.Recipient().Version()).Scan(&ok); err != nil {
					return
				}
				if ok {
					structures[tx.Recipient().Version()] = true
					continue
				}

				_, ok = structures[tx.Recipient().Version()]
				if ok {
					continue
				}
				structures[tx.Recipient().Version()] = true

				balances[string(tx.Sender().Bytes)] = balance - 50_000_00
				blk.AppendTransaction(tx)
			}
		case transaction.UpdateSmartContract:
			if err := pg.Db.QueryRow(ctx, "select exists (select 1 from structure_settings where version = $1 and master_address = $2)", tx.Recipient().Version(), tx.Sender().Bytes).Scan(&ok); err != nil {
				return
			}
			if !ok {
				continue
			}

			if err := pg.Db.QueryRow(ctx, "select exists (select 1 from structure_address where address = $1)", tx.Recipient().Bytes).Scan(&ok); err != nil {
				return
			}
			if ok {
				continue
			}

			blk.AppendTransaction(tx)
		case transaction.UpdateProfitAddress:
			if err := pg.Db.QueryRow(ctx, "select exists (select 1 from structure_settings where version = $1 and master_address = $2)", tx.Recipient().Version(), tx.Sender().Bytes).Scan(&ok); err != nil {
				return
			}
			if !ok {
				continue
			}

			if err := pg.Db.QueryRow(ctx, "select exists (select 1 from structure_address where address = $1)", tx.Recipient().Bytes).Scan(&ok); err != nil {
				return
			}
			if ok {
				continue
			}

			_, ok = addresses[string(tx.Recipient().Bytes)]
			if ok {
				continue
			}
			addresses[string(tx.Recipient().Bytes)] = true

			blk.AppendTransaction(tx)
		case transaction.UpdateFeeAddress:
			if err := pg.Db.QueryRow(ctx, "select exists (select 1 from structure_settings where version = $1 and master_address = $2)", tx.Recipient().Version(), tx.Sender().Bytes).Scan(&ok); err != nil {
				return
			}
			if !ok {
				continue
			}

			if err := pg.Db.QueryRow(ctx, "select exists (select 1 from structure_address where address = $1)", tx.Recipient().Bytes).Scan(&ok); err != nil {
				return
			}
			if ok {
				continue
			}

			_, ok = addresses[string(tx.Recipient().Bytes)]
			if ok {
				continue
			}
			addresses[string(tx.Recipient().Bytes)] = true

			blk.AppendTransaction(tx)
		case transaction.CreateTransitAddress:
			if err := pg.Db.QueryRow(ctx, "select exists (select 1 from structure_settings where version = $1 and master_address = $2)", tx.Recipient().Version(), tx.Sender().Bytes).Scan(&ok); err != nil {
				return
			}
			if !ok {
				continue
			}

			if err := pg.Db.QueryRow(ctx, "select exists (select 1 from structure_address where address = $1)", tx.Recipient().Bytes).Scan(&ok); err != nil {
				return
			}
			if ok {
				continue
			}

			_, ok = addresses[string(tx.Recipient().Bytes)]
			if ok {
				continue
			}
			addresses[string(tx.Recipient().Bytes)] = true

			blk.AppendTransaction(tx)
		case transaction.DeleteTransitAddress:
			if err := pg.Db.QueryRow(ctx, "select exists (select 1 from structure_settings where version = $1 and master_address = $2)", tx.Recipient().Version(), tx.Sender().Bytes).Scan(&ok); err != nil {
				return
			}
			if !ok {
				continue
			}

			if err := pg.Db.QueryRow(ctx, "select exists (select 1 from structure_address where address = $1 and type = 'transit')", tx.Recipient().Bytes).Scan(&ok); err != nil {
				return
			}
			if ok {
				blk.AppendTransaction(tx)
			}
		}
	}

	if rows.Err() != nil {
		return
	}

	rows.Close()

	if blk.TxCount() > 0 {
		var lstBlkHash []byte
		if err := pg.Db.QueryRow(ctx, "select hash from block order by height desc limit 1").Scan(&lstBlkHash); err != nil {
			return
		}

		blk.SetPreviousBlockHash(lstBlkHash)
		blk.SetTimestamp(uint32(now))
		blk.SetMerkleRootHash(blk.CalculateMerkleRoot())
		blk.Sign(key.NewSecretKey(secKey))

		if _, err := pg.Db.Exec(ctx, "select add_block($1)", blk.Bytes); err != nil {
			return
		}
	}
}
