module umi-master

go 1.14

require (
	github.com/btcsuite/btcutil v1.0.2
	github.com/jackc/pgx/v4 v4.6.0
	github.com/jackc/puddle v1.1.1 // indirect
	github.com/umi-top/umi-core v0.0.0-20200531171802-407bfa002cbb
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37 // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
)
