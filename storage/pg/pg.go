package pg

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	"os"
	"umi-master/storage/pg/schema"
	_ "umi-master/storage/pg/schema/routine"
	_ "umi-master/storage/pg/schema/table"
	_ "umi-master/storage/pg/schema/trigger"
	_ "umi-master/storage/pg/schema/type"
	_ "umi-master/storage/pg/schema/view"
)

var Db *pgxpool.Pool

func InitDb() {

	connString := os.Getenv("DB")
	if len(connString) == 0 {
		connString = "host=/tmp user=admin database=umi"
	}

	poolConfig, _ := pgxpool.ParseConfig(connString)
	poolConfig.ConnConfig.PreferSimpleProtocol = true

	var err error
	if Db, err = pgxpool.ConnectConfig(context.Background(), poolConfig); err != nil {
		log.Fatal("Unable to create connection pool: ", err)
	}

	if err = schema.InitSchema(Db); err != nil {
		log.Fatal("Could not init DB schema: ", err)
	}
}
