package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("add_block", addBlock)
}

func addBlock(ver int, tx pgx.Tx) (err error) {
	sql := `
create function add_block(bytes bytea)
    returns integer
    language plpgsql
as
$$
declare
    ver_genesis constant integer := 0;
    --
    blk_height           integer;
    blk_synced           boolean;
    --
    blk_hash             bytea;
    blk_version          smallint;
    blk_prv_hash         bytea;
    blk_merkle           bytea;
    blk_time             timestamptz;
    blk_tx_cnt           integer;
    blk_pubkey           bytea;
    --
    lst_blk_hash         bytea;
    lst_blk_height       integer;
begin
    select hash, version, prev_block_hash, merkle_root_hash, created_at, tx_count, public_key
    into blk_hash, blk_version, blk_prv_hash, blk_merkle, blk_time, blk_tx_cnt, blk_pubkey
    from parse_block_header(substr(bytes, 1, 167));

    select height, synced into blk_height, blk_synced from block where hash = blk_hash limit 1;

    if blk_synced is true then
        return blk_height;
    end if;

    if blk_synced is false then
        update block set synced = true where hash = blk_hash;
        return lo_from_bytea(blk_height, bytes);
    end if;

    if blk_version = ver_genesis then
        blk_height = 1;
    else
        select hash, height into lst_blk_hash, lst_blk_height from block order by height desc limit 1;

        if blk_prv_hash = lst_blk_hash then
            blk_height := lst_blk_height + 1;
        else
            return null;
        end if;
    end if;

    insert into block(hash, height, version, prev_block_hash, merkle_root_hash, created_at, tx_count, public_key, synced)
    values (blk_hash, blk_height, blk_version, blk_prv_hash, blk_merkle, blk_time, blk_tx_cnt, blk_pubkey, true);

    return lo_from_bytea(blk_height, bytes);
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
