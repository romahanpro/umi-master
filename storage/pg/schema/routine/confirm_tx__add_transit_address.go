package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("confirm_tx__add_transit_address", confirmTxAddTransitAddress)
}

func confirmTxAddTransitAddress(ver int, tx pgx.Tx) (err error) {
	sql := `
create function confirm_tx__add_transit_address(bytes bytea,
                                                tx_height integer,
												blk_height integer,
												blk_tx_idx integer,
												blk_time timestamptz)
    returns void
    language plpgsql
as
$$
declare
    tx_hash      bytea;
    tx_version   smallint;
    tx_sender    bytea;
    tx_recipient bytea;
    tx_struct    jsonb := '{}'::jsonb;
    st_version   integer;
    st_prefix    text;
begin
    select hash, version, sender, recipient, prefix
    into tx_hash, tx_version, tx_sender, tx_recipient, st_prefix
    from parse_transaction(bytes);

    st_version := convert_prefix_to_version(st_prefix);
    tx_struct := jsonb_set(tx_struct, '{prefix}', to_jsonb(st_prefix));

    insert into transaction (hash, height, confirmed_at, block_height, block_tx_idx, version, sender, recipient, struct)
    values (tx_hash, tx_height, blk_time, blk_height, blk_tx_idx, tx_version, tx_sender, tx_recipient, tx_struct);

    insert into structure_address (version, prefix, address, type, created_at, created_tx_height)
    values (st_version, st_prefix, tx_recipient, 'transit', blk_time, blk_height);

    --

    perform upd_address_balance(tx_recipient, 0::bigint, blk_time, tx_height, 'activate transit address', 'transit'::address_type);
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
