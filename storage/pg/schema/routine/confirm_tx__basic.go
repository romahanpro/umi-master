package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("confirm_tx__basic", confirmTxBasic)
}

func confirmTxBasic(ver int, tx pgx.Tx) (err error) {
	sql := `
create function confirm_tx__basic(bytes bytea,
                                             tx_height integer,
                                             blk_height integer,
                                             blk_tx_idx integer,
                                             blk_timestamp timestamptz)
    returns void
    language plpgsql
as
$$
declare
    umi_basic constant integer := x'55A9'::integer;
    --
    tx_hash            bytea;
    tx_ver             smallint;
    tx_sender          bytea;
    tx_recipient       bytea;
    tx_value           bigint;
    tx_fee_val         bigint;
    tx_fee_adr         bytea;
    --
    sender_ver         integer;
    recipient_ver      integer;
    --
    dev_address_       bytea;
    profit_address_    bytea;
    fee_address_       bytea;
    fee_percent_       smallint;
    --
begin
    select hash, version, sender, recipient, value
    into tx_hash, tx_ver, tx_sender, tx_recipient, tx_value
    from parse_transaction(bytes);

    perform upd_address_balance(tx_sender, -tx_value, blk_timestamp, tx_height, 'debit [sender]');

    sender_ver := (get_byte(tx_sender, 0) << 8) + get_byte(tx_sender, 1);
    if sender_ver <> umi_basic
    then
        select dev_address, profit_address, fee_address, fee_percent
        into dev_address_, profit_address_, fee_address_, fee_percent_
        from structure_settings
        where version = sender_ver
        limit 1;

        if tx_sender not in (dev_address_, profit_address_, fee_address_)
        then
            perform upd_structure_balance(sender_ver, -tx_value, blk_timestamp, tx_height, 'debit [structure]');
            perform upd_address_balance(dev_address_, -tx_value, blk_timestamp, tx_height, 'debit [dev]');
            perform upd_address_balance(profit_address_, -tx_value, blk_timestamp, tx_height, 'debit [profit]');
		elseif tx_sender = profit_address_
		then
            perform upd_address_balance(dev_address_, -tx_value, blk_timestamp, tx_height, 'debit [dev / profit]');
        end if;
    end if;

    recipient_ver := (get_byte(tx_recipient, 0) << 8) + get_byte(tx_recipient, 1);
    if recipient_ver <> umi_basic then
        select dev_address, profit_address, fee_address, fee_percent
        into dev_address_, profit_address_, fee_address_, fee_percent_
        from structure_settings
        where version = recipient_ver
        limit 1;

        if not exists(
                select 1
                from structure_address
                where (address = tx_recipient or address = tx_sender) 
                  and created_tx_height < tx_height
                  and (deleted_tx_height is null or deleted_tx_height > tx_height)
            )
        then
            if fee_percent_ > 0
            then
                tx_fee_val := ceil((tx_value::bigint * fee_percent_::bigint)::double precision / 10000)::bigint;
                tx_value := tx_value - tx_fee_val;
                tx_fee_adr := fee_address_;

                perform upd_address_balance(fee_address_, tx_fee_val, blk_timestamp, tx_height, 'credit [fee]');
            end if;
        end if;

        if tx_recipient not in (dev_address_, profit_address_, fee_address_)
        then
            perform upd_structure_balance(recipient_ver, tx_value, blk_timestamp, tx_height, 'credit [structure]');
            perform upd_address_balance(dev_address_, tx_value, blk_timestamp, tx_height, 'credit [dev]');
            perform upd_address_balance(profit_address_, tx_value, blk_timestamp, tx_height, 'credit [profit]');
        elseif tx_recipient = profit_address_
		then
            perform upd_address_balance(dev_address_, tx_value, blk_timestamp, tx_height, 'credit [dev / profit]');
        end if;
    end if;

    perform upd_address_balance(tx_recipient, tx_value, blk_timestamp, tx_height, 'credit [recipient]');

    insert into transaction (hash, height, confirmed_at, block_height, block_tx_idx, version, sender, recipient, value,
                             fee_address, fee_value)
    values (tx_hash, tx_height, blk_timestamp, blk_height, blk_tx_idx, tx_ver, tx_sender, tx_recipient, tx_value,
            tx_fee_adr, tx_fee_val);
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
