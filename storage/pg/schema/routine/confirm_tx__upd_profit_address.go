package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("confirm_tx__upd_profit_address", confirmTxUpdProfitAddress)
}

func confirmTxUpdProfitAddress(ver int, tx pgx.Tx) (err error) {
	sql := `
create function confirm_tx__upd_profit_address(bytes bytea,
                                              tx_height integer,
                                              blk_height integer,
                                              blk_tx_idx integer,
                                              blk_time timestamptz)
    returns void
    language plpgsql
as
$$
declare
    tx_hash      bytea;
    tx_version   smallint;
    tx_sender    bytea;
    tx_recipient bytea;
    tx_struct    jsonb := '{}'::jsonb;
    st_version   integer;
    st_prefix    text;
	--
    dev_adr      bytea;
    mst_adr       bytea;
    profit_adr   bytea;
    fee_adr      bytea;
    old_balance  bigint;
    new_balance  bigint;
    str_balance  bigint;
    --
    st_name       text;
    st_profit_prc smallint;
    st_fee_prc    smallint;
begin
    select hash, version, sender, recipient, prefix
    into tx_hash, tx_version, tx_sender, tx_recipient, st_prefix
    from parse_transaction(bytes);

    st_version := convert_prefix_to_version(st_prefix);
    tx_struct := jsonb_set(tx_struct, '{prefix}', to_jsonb(st_prefix));

    select value into str_balance from get_structure_balance(st_version, blk_time);

	select dev_address, profit_address, fee_address into dev_adr, profit_adr, fee_adr
    from structure_settings where version = st_version limit 1;

    if profit_adr = fee_adr
    then
		update structure_address
		set type = 'fee',
		    created_at = blk_time,
		    created_tx_height = confirm_tx__upd_profit_address.tx_height
		where version = st_version
		  and type = 'profit'
		  and deleted_at is null;

		select confirmed_value into old_balance from get_address_balance(profit_adr, blk_time);
		perform upd_address_balance(profit_adr, -str_balance, blk_time, tx_height, 'update profit [1]', 'fee'::address_type);
		perform upd_address_balance(dev_adr, -old_balance, blk_time, tx_height, 'update profit [1]', 'dev'::address_type);
        select confirmed_value into new_balance from get_address_balance(tx_recipient, blk_time);
		perform upd_structure_balance(st_version, -new_balance, blk_time, tx_height, 'update profit [1]');
		str_balance := str_balance - new_balance; 
		perform upd_address_balance(tx_recipient, str_balance, blk_time, tx_height, 'update profit [1]', 'profit'::address_type);
	else
		update structure_address
		set deleted_at = blk_time,
		    deleted_tx_height = confirm_tx__upd_profit_address.tx_height
		where version = st_version
		  and type = 'profit'
		  and deleted_at is null;

		select confirmed_value into old_balance from get_address_balance(profit_adr, blk_time);
		select confirmed_value into new_balance from get_address_balance(tx_recipient, blk_time);
		perform upd_address_balance(profit_adr, str_balance, blk_time, tx_height, 'update profit', 'deposit'::address_type);
		perform upd_structure_balance(st_version, -new_balance, blk_time, tx_height, 'update profit');
		perform upd_address_balance(dev_adr, -new_balance, blk_time, tx_height, 'update profit', 'dev'::address_type);
		perform upd_structure_balance(st_version, old_balance, blk_time, tx_height, 'update profit*');

		str_balance := str_balance + old_balance;
		perform upd_address_balance(tx_recipient, str_balance, blk_time, tx_height, 'update profit', 'profit'::address_type);
	end if;

	insert into structure_address (version, prefix, address, type, created_at, created_tx_height)
	values (st_version, st_prefix, tx_recipient, 'profit', blk_time, tx_height);

	update structure_settings
       set profit_address = tx_recipient,
           updated_at = blk_time,
           tx_height = confirm_tx__upd_profit_address.tx_height
    where version = st_version
    returning dev_address, master_address, fee_address, name, profit_percent, fee_percent
         into dev_adr, mst_adr, fee_adr, st_name, st_profit_prc, st_fee_prc;

    insert into structure_settings_log (version, prefix, name, profit_percent, fee_percent, dev_address, master_address, profit_address, fee_address, created_at, tx_height, comment)
    values (st_version, st_prefix, st_name, st_profit_prc, st_fee_prc, dev_adr, mst_adr, tx_recipient, fee_adr, blk_time, confirm_tx__upd_profit_address.tx_height, 'update profit');

	insert into transaction (hash, height, confirmed_at, block_height, block_tx_idx, version, sender, recipient, struct)
    values (tx_hash, tx_height, blk_time, blk_height, blk_tx_idx, tx_version, tx_sender, tx_recipient, tx_struct);
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
