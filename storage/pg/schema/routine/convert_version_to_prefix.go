package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("convert_version_to_prefix", convertVersionToPrefix)
}

func convertVersionToPrefix(ver int, tx pgx.Tx) (err error) {
	sql := `
create function convert_version_to_prefix(version integer)
    returns text
    language plpgsql
    immutable
as
$$
declare
    prefix bytea := E'\\x000000'::bytea;
begin
    if version = 0 then
        return 'genesis';
    end if;

    prefix := set_byte(prefix, 0, (((version & x'7C00'::integer) >> 10) + 96));
    prefix := set_byte(prefix, 1, (((version & x'03E0'::integer) >> 5) + 96));
    prefix := set_byte(prefix, 2, ((version & x'001F'::integer) + 96));

    return convert_from(prefix, 'LATIN1');
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
