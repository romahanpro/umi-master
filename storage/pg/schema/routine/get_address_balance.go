package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("get_address_balance", getAddressBalance)
}

func getAddressBalance(ver int, tx pgx.Tx) (err error) {
	sql := `
create or replace function get_address_balance(address bytea,
                                               epoch timestamptz default now()::timestamptz(0),
                                               composite boolean default true,
                                               out confirmed_value bigint,
                                               out confirmed_percent smallint,
                                               out unconfirmed_value bigint,
                                               out composite_value bigint,
                                               out type address_type)
    language plpgsql
as
$$
declare
    umi_basic   constant integer := x'55A9'::integer;
    adr_version integer := (get_byte(get_address_balance.address, 0) << 8) + get_byte(get_address_balance.address, 1);
    --
    rec                record;
    --
    periods   constant double precision := 30 * 24 * 60 * 60;
    nominal_interest   double precision;
    effective_interest double precision;
    period             double precision;
    --
    lst_value          bigint;
    lst_percent        smallint;
    lst_time           timestamptz;
    lst_type           address_type;
    --
    address_ver        integer;
    --
    lock_balance       bigint;
    profit_addr        bytea;
begin
    epoch := epoch::timestamptz(0);
    confirmed_value := 0::bigint;
    confirmed_percent := 0::smallint;
    unconfirmed_value := 0::bigint;

    select b.value, b.percent, b.updated_at, b.version, b.type
    into lst_value, lst_percent, lst_time, address_ver, lst_type
    from address_balance_confirmed b
    where b.address = get_address_balance.address
    limit 1;

    if lst_value is null
    then
		type := case adr_version when umi_basic then 'umi'::address_type else 'deposit'::address_type end;

        if type <> 'umi'::address_type
        then
			select deposit_percent into confirmed_percent
			from structure_percent_log
			where version = adr_version
			  and updated_at <= epoch order by updated_at desc limit 1;
			confirmed_percent := coalesce(confirmed_percent, 0::smallint);
		end if;

		return;
	end if;


    if lst_time > epoch
    then
        select b.value, b.percent, b.updated_at, b.version, b.type
        into lst_value, lst_percent, lst_time, address_ver, lst_type
        from address_balance_confirmed_log b
        where b.address = get_address_balance.address
          and updated_at <= epoch
        order by updated_at desc
        limit 1;
    end if;


    if lst_time is null then
        type := case adr_version when umi_basic then 'umi'::address_type else 'deposit'::address_type end;
		return;
	end if;
    --
    if address_ver <> umi_basic
    then
        for rec in
            select l.*
            from structure_percent_log l
            where l.version = address_ver
              and l.updated_at between lst_time and epoch
            loop
            	effective_interest := (lst_percent::double precision / 10000::double precision);
            	nominal_interest := periods * ((1::double precision + effective_interest) ^ (1::double precision / periods) - 1::double precision);
                period := extract(epoch from (rec.updated_at - lst_time));            	
                --
                lst_value := floor(lst_value::double precision * (1::double precision + (nominal_interest / periods)) ^ period)::bigint;
                lst_percent := case lst_type
                               when 'dev'::address_type then rec.dev_percent
                               when 'profit'::address_type then rec.profit_percent
                               when 'fee'::address_type then rec.profit_percent
                               else rec.deposit_percent end;
                lst_time := rec.updated_at;
            end loop;
        --
        period := extract(epoch from (epoch - lst_time));
        effective_interest := (lst_percent::double precision / 10000::double precision);
        nominal_interest := periods * ((1::double precision + effective_interest) ^ (1::double precision / periods) - 1::double precision);
        
        lst_value := floor(lst_value::double precision * (1::double precision + (nominal_interest / periods)) ^ period)::bigint;
        --
        if composite
        then
            if lst_type = 'profit'::address_type
            then
                select value into lock_balance from get_structure_balance(address_ver, epoch);
                composite_value := lst_value;
                lst_value := composite_value - lock_balance;
            elseif lst_type = 'dev'::address_type
            then
                select profit_address into profit_addr from structure_settings_log
                where version = adr_version and created_at <= epoch order by created_at desc limit 1;
                --
                select b.confirmed_value into lock_balance from get_address_balance(profit_addr, epoch, false) as b;
                composite_value := lst_value;
                lst_value := composite_value - lock_balance;
            end if;
        end if;
    end if;
    --
    confirmed_value := lst_value;
    unconfirmed_value := confirmed_value;
    confirmed_percent := lst_percent;
    type := lst_type;
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
