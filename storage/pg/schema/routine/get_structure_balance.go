package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("get_structure_balance", getStructureBalance)
}

func getStructureBalance(ver int, tx pgx.Tx) (err error) {
	sql := `
create or replace function get_structure_balance(version integer,
                                                 epoch timestamptz default now()::timestamptz(0),
                                                 out value bigint,
                                                 out percent smallint)
    language plpgsql
as
$$
declare
    rec         record;
    --
    periods   constant double precision := 30 * 24 * 60 * 60;
    nominal_interest   double precision;
    effective_interest double precision;
    period             double precision;
    --
    lst_value   bigint;
    lst_percent smallint;
    lst_time    timestamptz;
begin
    epoch := epoch::timestamptz(0);
    value := 0::bigint;
	percent := 0::smallint;
    --
    select b.value, b.percent, b.updated_at
    into lst_value, lst_percent, lst_time
    from structure_balance b
    where b.version = get_structure_balance.version
    limit 1;

	if lst_value is null
    then
		return;
	end if;

    if lst_time > epoch
    then
		select b.value, b.percent, b.updated_at
		into lst_value, lst_percent, lst_time
		from structure_balance_log b
		where b.version = get_structure_balance.version
		  and updated_at <= epoch
		order by updated_at desc
		limit 1;
	end if;

    if lst_time is null
    then
        return;
	end if;

    for rec in
        select l.*
        from structure_percent_log l
        where l.version = get_structure_balance.version
          and l.updated_at between lst_time and epoch
        loop
        	effective_interest := (lst_percent::double precision / 10000::double precision);
        	nominal_interest := periods * ((1::double precision + effective_interest) ^ (1::double precision / periods) - 1::double precision);
            period := extract(epoch from (rec.updated_at - lst_time));
            lst_value := floor(lst_value::double precision * (1::double precision + (nominal_interest / periods)) ^ period)::bigint;
            lst_percent := rec.deposit_percent;
            lst_time := rec.updated_at;
        end loop;
    --
    effective_interest := (lst_percent::double precision / 10000::double precision);
	nominal_interest := periods * ((1::double precision + effective_interest) ^ (1::double precision / periods) - 1::double precision);
	period := extract(epoch from (epoch - lst_time));
	lst_value := floor(lst_value::double precision * (1::double precision + (nominal_interest / periods)) ^ period)::bigint;
    --
    value := lst_value;
    percent := lst_percent;
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
