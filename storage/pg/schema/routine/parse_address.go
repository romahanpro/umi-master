package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("parse_address", parseAddress)
}

func parseAddress(ver int, tx pgx.Tx) (err error) {
	sql := `
create function parse_address(bytes bytea,
                                         out version integer,
                                         out public_key bytea)
    language plpgsql
as
$$
declare
    adr_length constant smallint := 34;
begin
    if length(bytes) != adr_length then
        raise exception 'address size should be % bytes', adr_length;
    end if;

    version := (get_byte(bytes, 0) << 8) + get_byte(bytes, 1);
    public_key := substr(bytes, 3, 32);
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
