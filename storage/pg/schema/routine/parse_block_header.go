package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("parse_block_header", parseBlockHeader)
}

func parseBlockHeader(ver int, tx pgx.Tx) (err error) {
	sql := `
create function parse_block_header(bytes bytea,
                                              out hash bytea,
                                              out version smallint,
                                              out prev_block_hash bytea,
                                              out merkle_root_hash bytea,
                                              out created_at timestamptz,
                                              out public_key bytea,
                                              out signature bytea,
                                              out tx_count integer)
    language plpgsql
as
$$
declare
    hdr_length constant  smallint := 167;
    ver_genesis constant integer := 0;
    blk_unixtime         integer;
begin
    if length(bytes) != hdr_length then
        raise exception 'block header size should be % bytes', hdr_length;
    end if;

    blk_unixtime := (get_byte(bytes, 65) << 24) + (get_byte(bytes, 66) << 16) +
                    (get_byte(bytes, 67) << 8) + get_byte(bytes, 68);

    hash := sha256(bytes);
    version := get_byte(bytes, 0);
    if version != ver_genesis then
        prev_block_hash := substr(bytes, 2, 32);
    end if;
    merkle_root_hash := substr(bytes, 34, 32);
    created_at := to_timestamp(blk_unixtime);
    public_key := substr(bytes, 72, 32);
    signature := substr(bytes, 104, 64);
    tx_count := (get_byte(bytes, 69) << 8) + get_byte(bytes, 70);
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
