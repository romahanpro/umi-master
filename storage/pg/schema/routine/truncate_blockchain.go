package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("truncate_blockchain", truncateBlockchain)
}

func truncateBlockchain(ver int, tx pgx.Tx) (err error) {
	sql := `
create function truncate_blockchain()
    returns void
    language plpgsql
as
$$
declare
    _sql text;
begin
    select into _sql string_agg(format('truncate table %s cascade;', tablename), E'\n')
    from pg_catalog.pg_tables
    where schemaname = 'public' and tablename not in ('level');
    if _sql is not null then
        execute _sql;
    end if;
    --
	perform setval('tx_height', 0, false);
    perform lo_unlink(lo.loid) from (select distinct loid from pg_largeobject) lo;
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
