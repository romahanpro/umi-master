package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("upd_structure_level", updStructureLevel)
}

func updStructureLevel(ver int, tx pgx.Tx) (err error) {
	sql := `
create or replace function upd_structure_level(block_height integer,
                                               block_time timestamptz,
                                               comment text default null)
    returns void
    language plpgsql
as
$$
    # variable_conflict use_column
declare
    rec         record;
    balance     bigint;
    cur_level   bigint;
    cur_percent smallint;
    upd_ver     integer;
begin
    --
    for rec in
        select version, prefix, profit_percent from structure_settings
        loop
            select value into balance from get_structure_balance(rec.version, block_time);
            --
            if balance is null
			then
				raise '% %', rec.version, rec.prefix;
			end if;
            --
            select id, percent
            into cur_level, cur_percent
            from level
            where balance between min_value and max_value
            limit 1;
            -- 
            update structure_percent
            set level           = cur_level,
                percent         = cur_percent,
                dev_percent     = case cur_level when 0 then 0 else cur_percent + 200 end,  -- 2%
                profit_percent  = cur_percent,
                deposit_percent = case cur_level when 0 then 0 else cur_percent - rec.profit_percent end,
                block_height    = upd_structure_level.block_height,
                updated_at      = upd_structure_level.block_time
            where version = rec.version
              and level <> cur_level
            returning version into upd_ver;
            --
            if upd_ver is not null
            then
                insert into structure_percent_log (version, prefix, level, percent, dev_percent, profit_percent, deposit_percent, block_height, updated_at, comment)
                values (
                        rec.version,  -- version
                        rec.prefix,   -- prefix
                        cur_level,    -- level
                        cur_percent,  -- percent
                        case cur_level when 0 then 0 else cur_percent + 200 end, -- dev_percent
                        cur_percent,  -- profit_percent
                        case cur_level when 0 then 0 else cur_percent - rec.profit_percent end, -- deposit_percent
                        upd_structure_level.block_height, -- block_height
                        upd_structure_level.block_time, -- updated_at
                        'level update' -- comment
                        );
            end if;
        end loop;
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
