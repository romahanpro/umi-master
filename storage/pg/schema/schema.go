package schema

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	"umi-master/global"
)

type schemas map[string]func(v int, tx pgx.Tx) error

var types = make(schemas)
var tables = make(schemas)
var routines = make(schemas)
var triggers = make(schemas)
var views = make(schemas)

func RegisterType(s string, f func(v int, tx pgx.Tx) error) {
	types[s] = f
}

func RegisterTable(s string, f func(v int, tx pgx.Tx) error) {
	tables[s] = f
}

func RegisterRoutine(s string, f func(v int, tx pgx.Tx) error) {
	routines[s] = f
}

func RegisterTrigger(s string, f func(v int, tx pgx.Tx) error) {
	triggers[s] = f
}

func RegisterView(s string, f func(v int, tx pgx.Tx) error) {
	views[s] = f
}

func InitSchema(db *pgxpool.Pool) (err error) {
	var curVer int = 3

	var tx pgx.Tx
	var dbVer int

	ctx := context.Background()

	if tx, err = db.Begin(ctx); err != nil {
		return err
	}

	defer func() { _ = tx.Rollback(ctx) }()

	if _, err = tx.Exec(ctx, "create sequence if not exists db_version minvalue 0 start 0 no cycle"); err != nil {
		return err
	}

	row := tx.QueryRow(ctx, "select setval('db_version', nextval('db_version'), false)")
	if err = row.Scan(&dbVer); err != nil {
		return err
	}

	if dbVer > curVer {
		return errors.New("outdated UMI node version")
	}

	if curVer > dbVer {
		if dbVer > 0 {
			sql := `
create or replace function reset_database()
    returns void
    language plpgsql
as
$$
declare
    _sql text;
begin
    select into _sql string_agg(format('drop function if exists %s cascade;', oid::regprocedure), E'\n')
    from pg_catalog.pg_proc
    where pronamespace = 'public'::regnamespace
      and prokind = 'f';
    if _sql is not null then
        execute _sql;
    end if;
    --
    select into _sql string_agg(format('drop view if exists %s cascade;', viewname), E'\n')
    from pg_catalog.pg_views
    where schemaname = 'public';
    if _sql is not null then
        execute _sql;
    end if;
    --
    select into _sql string_agg(format('drop table if exists %s cascade;', tablename), E'\n')
    from pg_catalog.pg_tables
    where schemaname = 'public'
      and tablename not in ('block');
    if _sql is not null then
        execute _sql;
    end if;
    --
    drop type if exists address_type cascade;
    --
    if exists(select 1 from information_schema.tables where table_schema = 'public' and table_name = 'block')
    then 
        update "block" set confirmed = false where confirmed is true;
    end if;
end
$$;
`
			if _, err = tx.Exec(ctx, sql); err != nil {
				return err
			}

			// зануляем базу
			log.Println("upgrade database")
			if _, err = tx.Exec(ctx, "select reset_database()"); err != nil {
				return err
			}
			log.Println("rescan blockchain")
		}

		// Накатываем схему
		log.Println("init database")
		for _, schema := range [...]schemas{types, tables, routines, triggers, views} {
			for _, f := range schema {
				if err = f(dbVer, tx); err != nil {
					return err
				}
			}
		}

		// добавляем genesis-блок
		if dbVer == 0 {
			log.Println("add genesis block")
			if _, err = tx.Exec(ctx, "select add_genesis($1)", global.TestNet); err != nil {
				return err
			}
		}

		if _, err = tx.Exec(ctx, "select setval('db_version', $1, false)", curVer); err != nil {
			return err
		}
	}

	return tx.Commit(ctx)
}
