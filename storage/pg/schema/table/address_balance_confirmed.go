package table

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterTable("address_balance_confirmed", addressBalanceConfirmed)
}

func addressBalanceConfirmed(ver int, tx pgx.Tx) (err error) {
	sql := `
create table address_balance_confirmed
(
    address            bytea        not null
        constraint address_balance_confirmed_pkey
            primary key,
    version            integer      not null,
    value              bigint       not null,
    percent            smallint     not null,
    type               address_type not null,
    created_tx_height  integer      not null,
    created_at         timestamptz  not null,
    tx_height          integer      not null,
    updated_at         timestamptz  not null,
    check (value >= 0 and percent >= 0)
);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}
	sql = `
create index address_balance_confirmed_idx
    on address_balance_confirmed (version);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
