package table

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterTable("address_balance_confirmed_log", addressBalanceConfirmedLog)
}

func addressBalanceConfirmedLog(ver int, tx pgx.Tx) (err error) {
	sql := `
create table address_balance_confirmed_log
(
    address     bytea        not null,
    version     integer      not null,
    value       bigint       not null,
    percent     smallint     not null,
    type        address_type not null,
    tx_height   integer      not null,
    updated_at  timestamptz  not null,
    delta_value bigint,
	comment     text
);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	sql = `
create index address_balance_confirmed_log_idx
    on address_balance_confirmed_log (address);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
