package table

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterTable("block", block)
}

func block(ver int, tx pgx.Tx) (err error) {
	if ver < 1 {
		sql := `
create table block
(
    hash             bytea                 not null
        constraint block_pkey
            primary key,
    height           integer               not null,

    version          smallint              not null,
    prev_block_hash  bytea
        constraint block_fk
            references block,
    merkle_root_hash bytea                 not null,
    created_at       timestamptz           not null,

    tx_count         integer               not null,
    public_key       bytea                 not null,

    synced           boolean default false not null,
    confirmed        boolean default false not null,
    constraint block_none_genesis
        check ((height > 0) and ((height = 1) or (prev_block_hash is not null)))
);
`
		if _, err = tx.Exec(context.Background(), sql); err != nil {
			return err
		}

		sql = `
create unique index block_height_uidx
    on block (height);
`
		if _, err = tx.Exec(context.Background(), sql); err != nil {
			return err
		}

		sql = `
create unique index block_prev_uidx
    on block (prev_block_hash);
`
		if _, err = tx.Exec(context.Background(), sql); err != nil {
			return err
		}

		sql = `
create index block_conf_idx
    on block (synced, confirmed)
    where ((synced is true) and (confirmed is false));
`
		if _, err = tx.Exec(context.Background(), sql); err != nil {
			return err
		}
	}

	return err
}
