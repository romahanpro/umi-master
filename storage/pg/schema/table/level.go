package table

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterTable("level", level)
}

func level(ver int, tx pgx.Tx) (err error) {
	sql := `
create table level
(
	id        smallint
		constraint level_pk
			primary key,
	min_value bigint   not null,
    max_value bigint   not null,
	percent   smallint not null
);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	sql = `
insert into level (id, min_value, max_value, percent) values
(0, 0, 4999999, 0),
(1, 5000000, 9999999, 1000),
(2, 10000000, 49999999, 1500),
(3, 50000000, 99999999, 2000),
(4, 100000000, 499999999, 2500),
(6, 500000000, 999999999, 3000),
(7, 1000000000, 4999999999, 3500),
(8, 5000000000, 9999999999, 3600),
(9, 10000000000, 49999999999, 3700),
(10, 50000000000, 99999999999, 3900),
(11, 100000000000, 9223372036854775807, 4100);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
