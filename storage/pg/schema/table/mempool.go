package table

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterTable("mempool", mempool)
}

func mempool(ver int, tx pgx.Tx) (err error) {
	sql := `
create table mempool
(
    hash       bytea
        constraint mempool_pk
            primary key,
    raw        bytea                     not null,
    priority   bigint                    not null,
    created_at timestamptz default now() not null
);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
