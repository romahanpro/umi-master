package table

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterTable("structure_address", structureAddress)
}

func structureAddress(ver int, tx pgx.Tx) (err error) {
	sql := `
create table structure_address
(
    version            smallint                 not null,
    prefix             char(3)                  not null,
    address            bytea                    not null,
    type               address_type             not null,
    created_tx_height  integer                  not null,
    deleted_tx_height  integer,
    created_at         timestamp with time zone not null,
    deleted_at         timestamp with time zone
);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
