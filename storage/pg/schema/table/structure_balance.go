package table

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterTable("structure_balance", structureBalance)
}

func structureBalance(ver int, tx pgx.Tx) (err error) {
	sql := `
create table structure_balance
(
    version    integer     not null
        constraint structure_balance_pk
            primary key,
    prefix     char(3)     not null,
    value      bigint      not null,
    percent    smallint    not null,
    tx_height  integer     not null,
    updated_at timestamptz not null,
    check (value >= 0 and percent >= 0)
);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
