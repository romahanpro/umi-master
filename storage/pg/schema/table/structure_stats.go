package table

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterTable("structure_stats", structureStats)
}

func structureStats(ver int, tx pgx.Tx) (err error) {
	sql := `
create table structure_stats
(
    version       integer                  not null
        constraint structure_stats_pk
            primary key,
    prefix        char(3)                  not null,
    address_count integer                  not null,
    updates_at    timestamp with time zone not null,
    tx_height     integer                  not null
);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
