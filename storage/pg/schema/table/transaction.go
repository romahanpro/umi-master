package table

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterTable("transaction", transaction)
}

func transaction(ver int, tx pgx.Tx) (err error) {
	sql := ` 
create table "transaction"
(
    hash         bytea       not null
        constraint transaction_pk
            primary key,
	height       integer,
	confirmed_at timestamptz not null,
    block_height integer,
    block_tx_idx integer,
    version      smallint    not null,
    sender       bytea       not null,
    recipient    bytea,
    value        bigint,
    fee_address  bytea,
    fee_value    bigint,
    struct       jsonb
);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	sql = `create sequence tx_height minvalue 0 start 0 no cycle owned by transaction.height`

	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
