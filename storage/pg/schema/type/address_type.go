package _type

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterType("address_type", addressType)
}

func addressType(ver int, tx pgx.Tx) (err error) {
	sql := `create type address_type as enum ('dev', 'master', 'profit', 'fee', 'transit', 'deposit', 'umi');`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
