package view

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterView("address_balance_confirmed_view", addressBalanceConfirmedView)
}

func addressBalanceConfirmedView(ver int, tx pgx.Tx) (err error) {
	sql := `
create view address_balance_confirmed_view as
select encode(address, 'hex')::text             as address,
       version,
       (value::numeric / 100)::money            as value,
       (percent::numeric / 100)::numeric(10, 2) as percent,
       tx_height,
       updated_at
from address_balance_confirmed;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
