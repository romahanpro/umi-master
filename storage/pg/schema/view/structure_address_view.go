package view

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterView("structure_address_view", structureAddressView)
}

func structureAddressView(ver int, tx pgx.Tx) (err error) {
	sql := `
create view structure_address_view as
select version,
       prefix,
       encode(address, 'hex')::text as address,
       type,
       created_at,
       deleted_at,
       (deleted_at is null)         as is_active
from structure_address;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
