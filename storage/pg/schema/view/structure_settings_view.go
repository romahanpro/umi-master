package view

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterView("structure_settings_view", structureSettingsView)
}

func structureSettingsView(ver int, tx pgx.Tx) (err error) {
	sql := `
create view structure_settings_view as
select version,
       prefix,
       name,
       (profit_percent::numeric / 100)::numeric(10, 2) as profit_percent,
       (fee_percent::numeric / 100)::numeric(10, 2)    as fee_percent,
       encode(dev_address, 'hex')::text                as dev_address,
       encode(master_address, 'hex')::text             as master_address,
       encode(profit_address, 'hex')::text             as profit_address,
       encode(fee_address, 'hex')::text                as fee_address,
       tx_height,
       updated_at
from structure_settings;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
