package view

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterView("transaction_view", transactionView)
}

func transactionView(ver int, tx pgx.Tx) (err error) {
	sql := `
create or replace view transaction_view as
select encode(hash, 'hex')::text                                      as hash,
       height,
       confirmed_at,
       block_height,
       block_tx_idx,
       version,
       encode(sender, 'hex')::text                                    as sender,
       encode(recipient, 'hex')::text                                 as recipient,
       (value::numeric / 100)::money                                  as value,
       encode(fee_address, 'hex')::text                               as fee_address,
       (fee_value::numeric / 100)::money                              as fee_value,
       struct ->> 'prefix'                                            as struct_prefix,
       struct ->> 'name'                                              as struct_name,
       ((struct ->> 'profit_percent')::numeric / 100)::numeric(10, 2) as struct_profit_percent,
       ((struct ->> 'fee_percent')::numeric / 100) ::numeric(10, 2)   as struct_fee_percent
from transaction;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
